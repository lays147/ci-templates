# CI Templates

This repository contains default templates to use on CI/CD pipelines
at Gitlab.

To use one of these templates create a `.gitlab-ci.yml` at your project and:

```yaml
include:
    - project: lays147/ci-templates
      file: Template.yml
```

You can use one or more of the templates. Just include them!

## AWS

Some of the templates of this repository are customized to run jobs in predefined AWS accounts.
They have [hidden jobs](https://docs.gitlab.com/ee/ci/yaml/README.html#hide-jobs) that are configured based on *environment vars*. Jobs are defined based on those hidden jobs by filling the information needed by the hidden job, the environment, and extra configurations. 

## Templates

### **DockerfileDind.yml**
This template builds and pushes Docker images to AWS ECR registry using _[Docker in Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker)_ method. There's one variable called `$SERVICE` that you need to manually set up on your  `.gitlab-ci.yml` with the given name of your application registry name so the job can push the image to the Docker repository.

### **DockerKaniko.yml**

This template builds and pushes Docker images to AWS ECR registry using _[Kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)_. There's one variable called `$SERVICE` that you need to manually set up on your  `.gitlab-ci.yml` with the given name of your application registry name so the job can push the image to the Docker repository.

 ### **HelmJobs.yml**

 This template use [Helm](https://helm.sh/) to apply changes in a Kubernetes Cluster. The configuration expects that the cluster is integrated with Gitlab.
 
 **Jobs**  
*helmDeploy*: It uses the `helm upgrade` command to deploy a new version/release of a chart. The following variables are expected on the job that implements this template:  
  - `RELEASE_NAME`: Name of your release  
  - `CHART_FOLDER`: Path to the chart folder or the chart name on a repository.*  
  - `IMAGE_TAG`: Tag of the image to be deployed  
  - `REPO`: Docker repository address  
  - `CUSTOM_ARGS`(optional): If you need to use more flags of helm upgrade, like a custom vars file: `-f variables-dev.yml`  
> \* If you are going to use a repository, you need to define a `before_script` section on your job to add the repository and update helm.


*helmHealthCheck*: It uses the `helm test` command to run the test defined on the chart. It expects `RELEASE_NAME` as a variable. It's also possible to skip this job by not defining it. If you are just extending the template, or if you use the `HelmJobs.yml` as a template, you can skip this step by setting `SKIP_HC` as true.

*helmRollback*: It uses the `helm rollback` command to rollback to the last release of the chart. It expects `RELEASE_NAME` as a variable.  

Check [HelmJobs](HelmJobs.yml) to see how the templates are built and examples of how to define jobs.

### **DeployLambda.yml**

This template makes an update in an [AWS Lambda](https://docs.aws.amazon.com/lambda/index.html) function. 
*lambdaDeploy* is the name of the job that you should extend. The job expects a variable called `LAMBDA_NAME` with the name of the function and a zip file called `lambda.zip` in the current path. 
